import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

if (process.env.NODE_ENV !== 'production') {
    var axe = require('react-axe');
    axe(React, ReactDOM, 1000);
}

let gameRenders = 0;
let boardRenders = 0;
let cursorRenders = 0;
let squareRenders = 0;
let moveRenders = 0;

class Cursor extends React.Component {
    render() {
        cursorRenders++;

        return (
            <div className="cursor" style={{ left: this.props.x, top: this.props.y }}>{ this.props.icon }</div>
        );
    }
}

// function component: only contain render method
class Square extends React.Component {

    // only re-render the square when the value/highlight properties updates
    // for optimisation
    shouldComponentUpdate(nextProps, nextState) {
        return this.props.value !== nextProps.value || this.props.highlight !== nextProps.highlight;
    }

    render() {
        squareRenders++;
        const cssClass = (this.props.highlight) ? 'square highlight' : 'square';

        // used to be button, not div, but wanted to try and modify cursor
        return (
            <div
                className={cssClass} 
                onClick={this.props.onClick}>
                {this.props.value}
            </div>
        );
    }
}

// class component
class Board extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            cursorX: -999,
            cursorY: -999
        }
    }

    positionElement = (event) => {
        const x = (event) ? event.nativeEvent.clientX : -999;
        const y = (event) ? event.nativeEvent.clientY : -999;

        this.setState({
            cursorX: x,
            cursorY: y
        });
    }

    handleMouseMove = (e) => {
        const _e = e;
        setTimeout(this.positionElement(_e), 1);
    }

    handleMouseLeave = (e) => {
        this.positionElement(null);
    }

    renderSquare(i) {
        const highlight = this.props.winningCombination && this.props.winningCombination.includes(i);
        return (
            <Square 
                key={'square_' + i}
                value={this.props.squares[i]} 
                highlight={highlight}
                onClick={() => this.props.onClick(i)} />
        );
    }

    renderGrid() {
        let grid = [];
        let squareNum = 0;

        for (var row = 0; row < this.props.boardSize; row++) {
            let squares = [];

            for (var square = 0; square < this.props.boardSize; square++) {
                squares.push(this.renderSquare( squareNum ) );
                squareNum++;
            }

            grid.push(<div key={'row_' + row} className="board-row">{squares}</div>);
        }

        return grid;
    }

    render() {
        boardRenders++;

        return (
            <div onMouseMove={this.handleMouseMove} onMouseLeave={this.handleMouseLeave}>
                {this.renderGrid()}
                <Cursor icon={this.props.cursorValue} x={this.state.cursorX} y={this.state.cursorY} />
            </div>
        );
    }
}

class ToggleMoves extends React.Component {
    render() {
        return (
            <button onClick={this.props.onClick}>
                Reverse Moves
            </button>
        )
    }
}

class Moves extends React.Component {
    renderMoveButton(move, desc) {
        return (
            <button onClick={() => this.props.jumpTo(move)}>
                { this.props.stepNumber === move ? (
                    <strong>{ desc }</strong>
                ) : (
                    <span>{ desc }</span>
                ) }
            </button>
        );
    }

    renderMoves() {
        let col = 0;
        let row = 0;
        const moves = this.props.history.map((step, move) => {
            const coords = this.props.history[move].moveCoords;
            col = (move && coords.length > 0) ? coords[0] : col;
            row = (move && coords.length > 0) ? coords[1] : row;
            let desc = move ?
                'Go to move #' + move + ', position: ' + col + ', ' + row :
                'Go to game start';
            return (
                <li key={move}>
                    {this.renderMoveButton(move, desc)}
                </li>
            );
        });
        return (this.props.reverse) ? moves.reverse() : moves;
    }

    render() {
        moveRenders++;
        return (
            <ol reversed={this.props.reverse}>
                {this.renderMoves()}
            </ol>
        )
    }
}

class Game extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            history: [{
                squares: Array(9).fill(null),
                moveCoords: [0,0] //col,row
            }],
            stepNumber: 0,
            xIsNext: true,
            reverseMoves: false,
            //win: null
        }

        //this.reverseMoves = this.reverseMoves.bind(this);
    }

    boardSize = 3; // 3x3

    handleClick(i) {
        const history = this.state.history.slice(0, this.state.stepNumber + 1);
        const current = history[history.length - 1];
        const squares = current.squares.slice();
        let moveCoords = current.moveCoords.slice();
        if (calculateWinner(squares) || squares[i]) {
            return;
        }
        // if (this.state.win || squares[i]) {
        //     return;
        // }
        
        let col = (i + 1) % this.boardSize;
        col = (col === 0) ? this.boardSize : col;
        let row = Math.ceil( (i + 1) / this.boardSize );
        moveCoords = [col, row];

        squares[i] = (this.state.xIsNext) ? 'X' : 'O';
        this.setState({ 
            history: history.concat([{ 
                squares: squares,
                moveCoords: moveCoords
            }]),
            stepNumber: history.length,
            xIsNext: !this.state.xIsNext,
            // win: calculateWinner(squares)
        });
    }

    jumpTo(step) {
        this.setState({
            stepNumber: step,
            xIsNext: (step % 2) === 0
        })
    }

    handleReverseMoves() {
        this.setState({
            reverseMoves: !this.state.reverseMoves
        });
    }

    render() {
        gameRenders++;
        const history = this.state.history;
        const current = history[this.state.stepNumber];
        const win = calculateWinner(current.squares);
        const winner = (win) ? win.winner : win;
        const winningCombination = (win) ? win.combination : win;

        // The win strictly speaking does not have to be in the state as the handleClick event triggers a re-render (setState), allowing the win to be calculated at that time.
        // // const win = calculateWinner(current.squares);
        // const winner = (this.state.win) ? this.state.win.winner : this.state.win;
        // const winningCombination = (this.state.win) ? this.state.win.combination : this.state.win;

        let status;
        if (winner) {
            status = 'Winner: ' + winner;
        } else if (current.squares.includes(null)) {
            status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
        } else {
            status = 'No winners, start a new game.';
        }

        return (
            <main className="game">
                <h1>Tic Tac Toe</h1>
                <div className="game-board">
                    <Board 
                        cursorValue={this.state.xIsNext ? 'X' : 'O'}
                        boardSize={this.boardSize}
                        squares={current.squares}
                        winningCombination={winningCombination}
                        onClick={(i) => this.handleClick(i)}    
                    />
                </div>
                <div className="game-info">
                    <div>{status}</div>
                    <ToggleMoves 
                        onClick={() => this.handleReverseMoves()} />
                    <Moves
                        reverse={this.state.reverseMoves}
                        history={this.state.history}
                        stepNumber={this.state.stepNumber}
                        jumpTo={(step) => this.jumpTo(step)} />
                    <div>Game renders: {gameRenders}</div>
                    <div>Board renders: {boardRenders}</div>
                    <div>Square renders: {squareRenders}</div>
                    <div>Move renders: {moveRenders}</div>
                    <div>Cursor renders: {cursorRenders}</div>
                </div>
            </main>
        );
    }
}
  
  // ========================================
  
ReactDOM.render(
    <Game />,
    document.getElementById('root')
);

// Helpers
function calculateWinner(squares) {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
        return { winner: squares[a], combination: [a, b, c] };
      }
    }
    return null;
}